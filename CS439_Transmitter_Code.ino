#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>
#include <PinChangeInterruptSettings.h>

// Set debug level
int debug = 2;

// Bit buffer for sending via LiFi
uint32_t sendBuffer = 0;

// const uint32_t  endSeq = 0b01101;
const uint32_t endSeq = 0b10110;

// Number of bits to send via LiFi
int bitCount = 0;
// Number of bits per cycle
const int codeLen = 3;
// Array to hold all bits to send
int myCode[codeLen] = {1};
// Previous bit sent (for NRTZ)
int prev = 1;

// Maximum message length per message
const int maxMessageLen = 32;
// Message buffer, storing message for resend
char message[maxMessageLen];
// Current message length in buffer
int messageLen = 0;
// Current byte in buffer
int curByte = 0;

// Number of clock cycles between messages
int messageDelay = -10;

// LED Pins
int rLEDPin= 11;
int gLEDPin = 6;
int bLEDPin = 5;
int iLEDPin = 3;

// Clock, ack, and calibration pins
int clockPin = 13;
int ackPin = 7;
int calPin = 8;

// Status if sending
int status = 0;

// Parity bytes and finshed sending flag 
uint32_t parity = 0;
int sentMessage = 0;

// Countdown for clock pin 
int clockCount = 1;

// Flag if in calibration mode
int calFlag = 0;
// Value of modulation strength
int strongFlag = -1;
// Scaling between modulation strengths
int scale = 85;

int benchmarking = 1;
long int timeStart;

// 4B -> 5B conversion
// Reverse order of 5B to make it easier to send
uint32_t encode[16] = {
  0b01111, 0b10010, 0b00101, 0b10101,
  0b01010, 0b11010, 0b01110, 0b11110,
  0b01001, 0b11001, 0b01101, 0b11101,
  0b01011, 0b11011, 0b00111, 0b10111
  };


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // Setting up pins
  pinMode(ackPin, INPUT_PULLUP);

  pinMode(calPin, OUTPUT);
  digitalWrite(calPin, 0);
  pinMode(clockPin, OUTPUT);
  
  pinMode(rLEDPin, OUTPUT);
  pinMode(gLEDPin, OUTPUT);
  pinMode(bLEDPin, OUTPUT);
  pinMode(iLEDPin, OUTPUT);

  // Initialized myCode array to zero
  for (int i = 0; i < codeLen; i++){
    myCode[i] = 3;
  }

  // Turn on all lights
  RGB_color(myCode[0], myCode[1], myCode[2], myCode[3]);

  // Attach interrupt to ACK pin
  attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(ackPin), ackMessage, CHANGE);

  cli();//stop interrupts
  
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = 150;// = (16*10^6) / (16*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10); 
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
 
  sei();//allow interrupts
}

ISR(TIMER1_COMPA_vect){
  // put your main code here, to run repeatedly:  

  // Clock signal
  if (clockCount == 0){
    
    clockCount = 1;    
    digitalWrite(clockPin, 1 - digitalRead(clockPin));

    if (sentMessage == 1){
      if (debug >= 1)
        Serial.println("\nMessage not ack\nResending\n");
      sentMessage = 0;
      curByte = 0;
      bitCount = 0;
    }

    if (calFlag) {
      // No Ack received, enter weak modulation mode
      setCalibrationLevel(0);
    }
  }
  clockCount--;
  
  // Reduces delay between messages
  if (messageLen < 0){
    messageLen++;
    return;
  }

  if (calFlag) {

    digitalWrite(calPin, 1);

    int seq[22][3] = {{0, 0, 0}, {3, 0, 0}, {0, 3, 0}, {0, 0, 3}, {3, 3, 3}, 
                     {3, 0, 0}, {3, 0, 3}, {3, 0, 1}, {3, 0, 2},
                     {0, 0, 0}, {0, 0, 3}, {0, 0, 1}, {0, 0, 2}, 
                     {0, 0, 3}, {3, 0, 3}, {1, 0, 3}, {2, 0, 3},
                     {0, 0, 0}, {3, 0, 0}, {1, 0, 0}, {2, 0, 0},
                     {3, 3, 3}};
    
    if (calFlag <= 22){
      myCode[0] = seq[calFlag - 1][0];
      myCode[1] = seq[calFlag - 1][1];
      myCode[2] = seq[calFlag - 1][2];
    }

    calFlag++;

    // Debug Code, Print Calibration Levels
    if (debug >= 3) {
      for (int i = 0; i < codeLen; i++) {
        Serial.print(myCode[i]);
        Serial.print("\t");
      }
      Serial.println();
    }
    RGB_color(myCode[0], myCode[1], myCode[2], myCode[3]);

    if (Serial.peek() == '~') {
      Serial.read();
    }

    messageLen = 0;
    return;
  }

  // Read data from Serial when available
  if (Serial.available() > 0 && messageLen < maxMessageLen && messageLen >= 0 && strongFlag != -1){

    int incoming = Serial.read();
    char c = incoming;
    message[messageLen++] = c;
  
    if (debug >= 2) {
      Serial.print("\t\t\tI received: ");
      Serial.print(incoming);
      Serial.print(" ");
      Serial.print(c);
      Serial.print(" ");
      Serial.print(incoming, BIN);
      Serial.println("   ");
    }
  }

  // Enter Calibration mode
  if ((messageLen > 0 && message[0] == '~') || (strongFlag == -1 && Serial.available())) {
    calFlag = 1;
    
    if (debug >= 2)
      Serial.println("Calibrating");
    
    clockCount = 25;
    strongFlag = 1;
    scale = 85;
    return;
  }

  // Start Byte message
  if (curByte == 0 && messageLen > 0 && !calFlag){
    // Send start frame
    sendBuffer = 0b00011;
    bitCount = 5;

    // Reset parity bits
    parity = 0;

    timeStart = millis();
  }

  // End Byte message
  if (bitCount <= 2 * codeLen && curByte == messageLen && messageLen > 0 && sentMessage == 0){

    // Send parity message
    sendBuffer = sendBuffer | (parity << bitCount);
    bitCount += 10;

    // Send end frame
    sendBuffer = sendBuffer | (endSeq << bitCount);
    bitCount += 10;

    // Countdown for message ACK
    clockCount = 50;    
    sentMessage = 1;
  }

  // From message buffer to sendBuffer
  if (bitCount <= 2 * codeLen && curByte < messageLen) {

    int incoming = message[curByte++];

    uint32_t temp = encode[ (incoming & 0b11110000) >> 4 ];
    sendBuffer = sendBuffer | (temp << bitCount);
    bitCount += 5;
    parity = parity ^ (temp << 0);

    temp = encode[ (incoming & 0b1111) ];
    sendBuffer = sendBuffer | (temp << bitCount);
    bitCount += 5;
    parity = parity ^ (temp << 5);
  }

  // Send message
  if (bitCount > 0){

    int numColor = strongFlag == 1 ? 2 : 3;

    // NRTZ to output buffer
    for (int i = 0; i < numColor; i++){
      int strength = 0;

      for (int j = 0; j < 1 + strongFlag; j++) {
        strength = strength << 1;
        
        int bit = sendBuffer & 0b1;
        prev = bit ^ prev;

        sendBuffer = sendBuffer >> 1;
        bitCount--;

        strength = strength | prev;
      }

      myCode[i] = strength;        
    }
    
    // Debug Code, Print outgoing levels
    if (debug >= 1) {
      for (int i = 0; i < numColor; i++) {
        if (strongFlag == 1 && myCode[i] < 2){
          Serial.print("0");
        }
        Serial.print(myCode[i], BIN);
        Serial.print("\t");
      }
      Serial.println();
    }

    if (strongFlag == 0) {
      RGB_color(myCode[0], myCode[1], myCode[2], myCode[3]);
    } else {
      RGB_color(myCode[0], myCode[2], myCode[1], myCode[3]);
    }
  }
}

void ackMessage(){
  if (calFlag){
    setCalibrationLevel(1);
  } else if (sentMessage) {
    if (debug >= 1)
      Serial.println("Message ACKNOWLEDGED");

    if (benchmarking) {
      long int timeTaken = millis() - timeStart;
      double bw = (double) messageLen * 1000 / timeTaken;
      Serial.print("Time Taken: ");
      Serial.print(timeTaken);
      Serial.println("ms");
      Serial.print("Number of chars sent: ");
      Serial.println(messageLen);
      Serial.print("Bandwidth is ");
      Serial.print(bw);
      Serial.println(" Bytes/Second");
      Serial.println("\n");
    }

    sentMessage = 0;
    messageLen = messageDelay;
    curByte = 0;
  }
}

void loop(){
  delay(1000);
}

void RGB_color(int rVal, int gVal, int bVal, int iVal) {
  analogWrite(rLEDPin, rVal * scale);
  analogWrite(gLEDPin, gVal * scale);
  analogWrite(bLEDPin, bVal * scale);
  analogWrite(iLEDPin, iVal * scale);
}

void setCalibrationLevel(int level) {
  digitalWrite(calPin, 0);
  if (debug >= 0) {
    Serial.print("Calibration Complete: ");
    if (level == 1)
      Serial.println("Strong\n");
    else
      Serial.println("Weak\n");
  }
  strongFlag = level;

  scale = 255 / (2 * level + 1);
  
  calFlag = 0;
  messageLen = messageDelay;

  if (level == 1) {
    myCode[2] = 0;
    myCode[3] = 0;
    RGB_color(myCode[0], myCode[2], myCode[1],myCode[3]);
  }
}
