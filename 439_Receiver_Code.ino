#include <Wire.h>
#include "Adafruit_TCS34725.h"
bool DEBUG = false;//true;

int Clock = 3; 
int ACK = 4; 
int CALIBRATE = 2;
int currACK = 1;
int timeLength = 150;

int CALIBRATE_LENGTH = 21;//8;

int CALIBRATE_SEQUENCE[] = {0,0b00110000,0b00001100,0b00000011,0b00111111,0b00110000,0b00110011,0b00110001,0b00110010,0b00000000,0b00000011,0b00000001,0b00000010,0b00000011,0b00110011,0b00010011,0b00100011,0b00000000,0b00110000,0b00010000,0b00100000};

int COLORS = 3;
int LEVELS = 4;

int CURR_LEVELS = 2;

int REQUIRED_SEP  = 20;

/* Example code for the Adafruit TCS34725 breakout library */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 3.3V DC
   Connect GROUND to common ground */

/* Initialise with default values (int time = 2.4ms, gain = 1x) */
// Adafruit_TCS34725 tcs = Adafruit_TCS34725();

/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725();
bool toggled = false;
uint8_t conversion4b_5b[16] = {0b11110,0b01001,0b10100,0b10101,0b01010,0b01011,0b01110,0b01111,0b10010,0b10011,0b10110,0b10111,0b11010,0b11011,0b11100,0b11101};
uint8_t conversion5b_4b[32];

bool isCalibrating = false;
int calibrateIndex = 0;
int mins[][4] = {{1024,1024,1024,1024},{1024,1024,1024,1024},{1024,1024,1024,1024}};
int maxs[][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
int avgs[][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
void setup(void) {
  Serial.begin(9600);

  if (tcs.begin()) {
    Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }
  for (int i = 0; i < 32; i++) {
    conversion5b_4b[i] = 0b11111111;
  }
  for (int i = 0; i < 16; i++) {
    conversion5b_4b[conversion4b_5b[i]] = i;
  }
  pinMode (Clock, INPUT_PULLUP); // sensor pin INPUT
  attachInterrupt(digitalPinToInterrupt(Clock), clock_trigger, CHANGE);

  pinMode (CALIBRATE, INPUT_PULLUP); // sensor pin INPUT
  attachInterrupt(digitalPinToInterrupt(CALIBRATE), calibrate_trigger, CHANGE);

  pinMode (ACK, OUTPUT); // sensor pin INPUT
  digitalWrite(ACK,currACK);
  delay(1000);

  cli();

  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = timeLength;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts
}

void clock_trigger() {
  TCNT1 = (int) timeLength / 2;
}

void calibrate_trigger() {
  Serial.println("Calibrate=======================");

  int calibrate = digitalRead(CALIBRATE);

  if (calibrate) {
    isCalibrating = true;
    calibrateIndex = 0;
    COLORS = 3;
    for (int i = 0; i < COLORS; i++) {
      for (int j = 0; j < LEVELS; j++) {
          mins[i][j] = 1024;
          maxs[i][j] = 0;        
      }
    }
  } else {
    isCalibrating = false;
  }
}

ISR(TIMER1_COMPA_vect) {
  toggled = true;
}

uint16_t r, g, b, c;

uint16_t last_read_bits = 0;
uint16_t last_calculated_bits = 0;
void runCalibrate() {
  if (DEBUG) {
    Serial.print("RUNNING CALIBRATE ");
    Serial.println(calibrateIndex);
  }

  if (calibrateIndex < CALIBRATE_LENGTH) {
    tcs.getRawData(&r, &g, &b, &c);

    if (DEBUG) {
      Serial.print(r);
      Serial.print('\t');
      Serial.print(g);
      Serial.print('\t');
      Serial.print(b);
      Serial.println('\t');
    }

    int cols[3] = {r,g,b};
    for(int a = 0; a < COLORS; a++) {
      int ind = 0;
      ind = ((CALIBRATE_SEQUENCE[calibrateIndex] >> ((2-a)*2)) & 0b11 )
      // switch ((CALIBRATE_SEQUENCE[calibrateIndex] >> ((2-a)*2)) & 0b11 )
      // {
      //   case 0b00: ind = 0;
      //     break;
      //   case 0b01: ind = 1;
      //     break;
      //   case 0b10: ind = 2;
      //     break;
      //   case 0b11: ind = 3;
      //     break;
      //   default: ind = 0;
      //     break;
      // }
      if (cols[a] < mins[a][ind]) {
        mins[a][ind] = cols[a];
      }
      if (cols[a] > maxs[a][ind]) {
        maxs[a][ind] = cols[a];
      }
    }
    calibrateIndex++;
  } else {
    if (DEBUG) {
      for (int i = 0; i < COLORS; i++) {
        for (int j = 0;j<LEVELS;j++) {
          Serial.print(mins[i][j]);
          Serial.print("\t"); 
        }     
        Serial.println("\t");
        for (int j = 0; j < LEVELS; j++) {
          Serial.print(maxs[i][j]);
          Serial.print("\t"); 
        }
        Serial.println("\t");
      }
    }

    isCalibrating = false;
    calibrateIndex = 0;
    if (DEBUG) {
      Serial.print("AVERAGES ");
    }
    bool good = true;
    for (int a = 0; a < COLORS; a++) {
      for (int l = 0; l < LEVELS - 1;l++) {
        avgs[a][l] = (int)(mins[a][l+1] + maxs[a][l])/2;
        if (mins[a][l+1] - maxs[a][l] < REQUIRED_SEP) {
          good = false;
        }
        if (DEBUG) {
          Serial.print(avgs[a][l]);
          Serial.print("\t");
        }
      }
      avgs[a][3] = (int)(mins[a][3] + maxs[a][0])/2;
      if (DEBUG) {
        Serial.print(avgs[a][3]);
        Serial.print("\t");
        Serial.println("\t");
      }
    }

    if (good) {
      if (currACK == 0) {
        currACK = 1;
      } else {
        currACK = 0;
      }
      digitalWrite(ACK,currACK);
      CURR_LEVELS = 4;
      COLORS = 2;
      Serial.println("HIGH POWER");
    } else {
      CURR_LEVELS = 2;
      COLORS = 3;
      Serial.println("LOW POWER");
    }
  }
}

void getSignal() {
  tcs.getRawData(&r, &g, &b, &c);
  int calibrate = digitalRead(CALIBRATE);

  int cols[3] = {r,g,b};
  if (COLORS == 2) {
    cols[0] = r;
    cols[1] = b;
  }

  for (int i = 0; i < COLORS; i++) {
    if (CURR_LEVELS == 2) {
      if(cols[i] > avgs[i][3]) {
        last_read_bits = (last_read_bits << 1) | 1;
        checkLastBytes();
      } else {
        last_read_bits = (last_read_bits << 1) | 0;
        checkLastBytes();
      }
    } else {
      int c = i;
      if (c == 1) {
        c = 2;
      }
      int a = 0;
      int b = 0;
      if (cols[c] < avgs[c][0]) {
          a = 0;
          b = 0;
      } else if (cols[c] < avgs[c][1]) {
        a = 0;
        b = 1;
      } else if(cols[c] < avgs[c][2]) {
        a = 1;
        b = 0;
      } else {
        a = 1;
        b = 1;
      }

      last_read_bits = (last_read_bits << 1) | a;
      checkLastBytes();
      last_read_bits = (last_read_bits << 1) | b;
      checkLastBytes();
    }
  }
  /*if(r > avgs[0][0])
  {
    last_read_bits = (last_read_bits << 1) | 1;
  }
  else
  {
    last_read_bits = (last_read_bits << 1) | 0;
  }
  checkLastBytes();

  if(g > avgs[1][0])//40
  {
    last_read_bits = (last_read_bits << 1) | 1;
  }
  else
  {
    last_read_bits = (last_read_bits << 1) | 0;
  }
  checkLastBytes();

  if(b > avgs[2][0])
  {
    last_read_bits = (last_read_bits << 1) | 1;
  }
  else
  {
    last_read_bits = (last_read_bits << 1) | 0;
  }

  checkLastBytes();
  /*if(irStatus == 1)
  {
    last_read_bits = (last_read_bits << 1) | 1;
  }
  else
  {
    last_read_bits = (last_read_bits << 1) | 0;
  }

  checkLastBytes();*/

  if (DEBUG) {
    Serial.print((last_read_bits & 0b100) >> 2);
    Serial.print('\t');
    Serial.print((last_read_bits & 0b10) >> 1);
    Serial.print('\t');
    Serial.println(last_read_bits & 0b1);
    Serial.print(r);
    Serial.print('\t');
    if(CURR_LEVELS == 2) {
      Serial.print(g);
      Serial.print('\t');
    }
    Serial.print(b);
    Serial.print('\t');
    Serial.print(currACK);
    Serial.print('\t');
    Serial.print(calibrate);
    Serial.println('\t');
  }
}

void loop(void) {
  if (toggled) {
    toggled = false;
    if (isCalibrating) {
      runCalibrate();
    }
    else {
      getSignal();
    }
  }
}

uint16_t mask = 0b0000000000111110;
uint16_t c_mask = 0b0000011111111110;
uint8_t frame_start = 0b11000;
uint8_t frame_end = 0b01101;

bool inFrame = false;
int count = 0;
int currentCharInt = 0;
int16_t read10Bits[60];
char readChars[60];
int16_t runningTotal = 0;
bool mustBeFrameEnd = false;

void checkLastBytes() {
  last_calculated_bits = (last_read_bits) ^ (last_read_bits << 1);
  count++;
  uint16_t masked = (last_calculated_bits & mask)>>1;
      /*Serial.print("Check ");
      Serial.print(inFrame);
      Serial.print("\t");
      Serial.println(count);*/

  if (mustBeFrameEnd && count == 5 && masked != frame_end) {
    Serial.println("ERROR: FRAME END WITH INCORRECT PARITY");
    inFrame = false;
    currentCharInt = 0;
    count = 0;
    runningTotal = 0;
    mustBeFrameEnd = false;
    last_read_bits = 0;

    return;
  }
  if (masked == frame_start && !inFrame) {
    if(inFrame) {
      Serial.println("ERROR: SECOND FRAME START");
    } else {
      Serial.println("FRAME START");
      inFrame = true;
      currentCharInt = 0;
      count = 0;
      return;
    }
  } else if (inFrame && count == 10) {
    read10Bits[currentCharInt] = 0;
    read10Bits[currentCharInt] |= (last_read_bits & c_mask)>>1; 
    masked = (last_calculated_bits & mask)>>1;
    if (DEBUG) {
      Serial.println("CHAR");
      Serial.println(masked,BIN);
    }

    readChars[currentCharInt] = conversion5b_4b[masked];   

    masked = (last_calculated_bits & (mask << 5))>>6;
    if (DEBUG) {
      Serial.println(masked,BIN);
    }

    readChars[currentCharInt] |= conversion5b_4b[masked] << 4;   
    currentCharInt++;

    runningTotal = runningTotal ^ ((last_calculated_bits & c_mask)>>1);

    count = 0;
  } else if (masked == frame_end && count == 5) {
    if (DEBUG) {
      Serial.println("END");
    }
    if (inFrame && currentCharInt > 0) {
      bool should = false;
      if(runningTotal == 0) {
        if(currACK == 0) {
          currACK = 1;
        } else {
          currACK = 0;
        }
        digitalWrite(ACK,currACK);
        readChars[currentCharInt - 1] = '\n';
        readChars[currentCharInt] = '\0';
        should = true;
      } else { 
        //Could be a parity frame end, give it another chance
        mustBeFrameEnd = true;
        Serial.println("Frame end but not valid parity, maybe wait a char");
      }
      Serial.println(readChars);
      Serial.println("Printing read bits:");
      for (int q = 0; q < currentCharInt; q++) {
        Serial.println(read10Bits[q],BIN);
      }
      if (should) {
        Serial.println("FRAME END");
        currentCharInt = 0;
        inFrame = false;
        count = 0;
        last_read_bits = 0;
        mustBeFrameEnd = false;
        runningTotal = 0;
      }
    } else {
      if (DEBUG) {
        Serial.println("INVALID FRAME END");
      }
      inFrame = false;
      currentCharInt = 0;
      count = 0;
      runningTotal = 0;
      mustBeFrameEnd = false;
    }
  } 

  if(currentCharInt > 64) {
    currentCharInt = 0;
    inFrame = false;
    count = 0;
    runningTotal = 0;
    mustBeFrameEnd = false;
  }
}
